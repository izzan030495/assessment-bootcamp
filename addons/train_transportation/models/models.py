from odoo import models, fields, api
from datetime import timedelta, datetime, date

class TrainTrain(models.Model):
    _name = 'train.trains'
    _description = 'Train Train'

    name = fields.Char(string='Name Train', required=True)
    code = fields.Char(string='Code Train')
    capacity = fields.Char(string='Capacity Train')
    state = fields.Selection([('ready', 'Ready'), ('not ready', 'Not Ready'), ('maintenance', 'Maintenance')], string='State Train', required=True)
    schedule_id = fields.Many2one('train.schedule', string='Schedule Train', required=True, ondelete='cascade', readonly=True)
    

    # @api.depends('value')
    # def _value_pc(self):
    #     for record in self:
    #         record.value2 = float(record.value) / 100

class TrainSchedule(models.Model):
    _name = 'train.schedule'
    _description = 'Train Schedule'

    ref = fields.Char(string='Referensi', readonly=True, default='/')
    trains_line = fields.One2many('train.trains', 'schedule_id', string='Sesi', tracking=True)
    start_date = fields.Date(string='Tanggal')
    duration = fields.Float(string='Durasi', help='Lama Perjalanan')
    end_date = fields.Date(string="Arrival Time", compute='get_end_date', inverse='set_end_date', store=True, readonly=True)

    @api.model
    def create(self, vals):
        vals['ref'] = self.env['ir.sequence'].next_by_code('train.schedule')
        return super(TrainSchedule, self).create(vals)
    
    @api.depends('start_date', 'duration')
    def get_end_date(self):
        for sesi in self:
            if not sesi.start_date: 
                sesi.end_date = sesi.start_date
                continue

            start = fields.Date.from_string(sesi.start_date)
            sesi.end_date = start + timedelta(days=sesi.duration)
 
    def set_end_date(self):
        for sesi in self:
            if not (sesi.start_date and sesi.end_date):
                continue
 
            start_date = fields.Datetime.from_string(sesi.start_date)
            end_date = fields.Datetime.from_string(sesi.end_date)
            sesi.duration = (end_date - start_date).days + 1
